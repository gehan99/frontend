import './App.css';
import NavigationBar from './component/NavigationBar';
import {Route,BrowserRouter,Routes} from 'react-router-dom'
import LandingPage from './component/LandingPage';
import Profile from './component/ProfilePage';

function App() {
  return (
    <div >
      <BrowserRouter>
      <NavigationBar/>
      <Routes>
      <Route path="/"element={<LandingPage/>}></Route>
      <Route path="/profile"element={<Profile/>}></Route>
      </Routes>
      </BrowserRouter>


    </div>
  );
}

export default App;
