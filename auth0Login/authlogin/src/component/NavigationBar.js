import React from "react";
import { Link } from "react-router-dom"
// import { FaShoppingCart } from "react-icons/fa";
import { AiOutlineAlibaba } from "react-icons/ai";
import { FcShop } from "react-icons/fc";

import { useAuth0 } from '@auth0/auth0-react'

const NavigationBar = () => {
  const { loginWithRedirect, isAuthenticated, logout, user } = useAuth0()
  return (
    <>
      {console.log(JSON.stringify(user, null, 2))}
      <nav className="navbar navbar-expand-lg navbar-dark " style={{backgroundColor:'black'}}>
        <div className="container-fluid">
          <a className="navbar-brand" ><AiOutlineAlibaba size='2rem' color="orange" />Alibaba</a>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarNavDropdown">
            <ul className="navbar-nav">
              <li className="nav-item">
                <Link className="nav-link active" aria-current="page" to='/' >Main</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" aria-current="page" to='/profile' >Profile</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" aria-current="page" to='/profile' >About Us</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" aria-current="page" to='/profile' >ESG</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" aria-current="page" to='/profile' >New and Resources</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" aria-current="page" to='/profile' >Career</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" aria-current="page" to='/' ></Link>
              </li>
            </ul>
          </div>
          <div className="collapse navbar-collapse nav justify-content-end" id="navbarNavDropdown">
            <ul className="navbar-nav">
              {isAuthenticated ?
                <li className="nav-item">
                  <Link className="nav-link active" aria-current="page" >Hi.. {user.name}</Link>
                </li> : false}
              {isAuthenticated ?
                (<li className="nav-item">
                  <Link className="nav-link" onClick={() => logout()} >Logout</Link>
                </li>) : (<li className="nav-item">
                  <Link className="nav-link" style={{color:'red'}} onClick={() => loginWithRedirect()}>Login</Link>
                </li>)}
              <li className="nav-item">
                {/* <Link className="nav-link" to='/cart'><FaShoppingCart size='2rem' color="white" /></Link> */}
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  )
}
export default NavigationBar;