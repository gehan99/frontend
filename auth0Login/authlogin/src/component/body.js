import React from "react";

const Body = () => {
    return (
        <>
            <div className="container-fluid" >
                <div className="row">
                    <div className="col"><div className="card" style={{ width: '92%',background:'orange' }}>
                        <img className="card-img-top" src="https://assets.pandaily.com/uploads/2022/08/%E9%A3%9E%E4%B9%A620220829-180410.jpg" alt="Card image cap" />
                        <div className="card-body">
                            <p className="card-text">
                                Alibaba Cuts Carbon Footprint by 620,000t With Use Of Renewables: Report</p>
                        </div>
                    </div></div>
                    <div className="col-4"><div className="card" style={{ width: '82%' ,height:'100%' ,background:'orange'}}>
                        <img className="card-img-top" src="https://wwd.com/wp-content/uploads/2022/09/Screen-Shot-2022-09-21-at-11.36.22-AM-1.png" alt="Card image cap" />
                        <div className="card-body">
                            <p className="card-text">
                                Alibaba Rolls Out Deeper Immersive Luxury Shopping Experience In The Metaverse</p>
                        </div>
                    </div></div>
                    

                </div>
                <div className="row" style={{ paddingTop: "2%" }}>
                    <div className="col-4"><div className="card" style={{ width: '82%',height:'100%' ,background:'orange'}}>
                        <img className="card-img-top" src="https://alizila.oss-us-west-1.aliyuncs.com/uploads/2022%2F09%2Fpatient-min.png" alt="Card image cap" />
                        <div className="card-body">
                            <p className="card-text">
                                Alibaba Deploys Artificial Intelligence To Help Bridge Digital Divide</p>
                        </div>
                    </div></div>
                    <div className="col-4"><div className="card" style={{ width: '82%', height:'100%',background:'orange' }}>
                        <img className="card-img-top" src="https://alizila.oss-us-west-1.aliyuncs.com/uploads/2022/08/Alibaba-Maker-Festival-2022-1.jpeg" alt="Card image cap" />
                        <div className="card-body">
                            <p className="card-text">
                                Alibaba’s Maker Festival Shines Spotlight On Emerging Entrepreneurs and Consumption Trends in China</p>
                        </div>
                    </div></div>
                    <div className="col-4"><div className="card" style={{ width: '82%',height:'100%',background:'orange' }}>
                        <img className="card-img-top" src="http://www.theborneopost.com/newsimages/2018/09/daniel-zhang.jpg" alt="Card image cap" />
                        <div className="card-body">
                            <p className="card-text">
                                Alibaba Group Chairman Daniel Zhang’s 2022 Letter to Shareholders</p>
                        </div>
                    </div></div>

                </div>
            </div></>
    )
}
export default Body