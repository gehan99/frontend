import React from "react";
import {useAuth0} from '@auth0/auth0-react'

const Profile =()=>{
    const {user,isAuthenticated}= useAuth0()
    // console.log("user data",user)

    return(
        <>

        {isAuthenticated?
        <div style={{
            position: 'absolute', left: '50%', top: '50%',
            transform: 'translate(-50%, -50%)'
        }}>
            <div className="card shadow-sm " style={{ width: '30rem' }}>
                <div className="card-body">
                    {/* <h5 className="card-title">SIGN IN</h5> */}
                    <p className="card-text"></p>
                    <form>
                       
                    <img src={user.picture} className="rounded-circle" style={{width: '150px'}} alt="Avatar" />
                        
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Email address</label>
                            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
                        </div>
                        {console.log(JSON.stringify(user, null, 2))}
                        
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Password</label>
                            <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
                        </div>
                        
                        <br/>
                        <button type="submit" className="btn btn-primary">Login</button>
                    </form>
                </div>
            </div>
        </div>:false
        
    }

        
        </>
    )
}

export default Profile