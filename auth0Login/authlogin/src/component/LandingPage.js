import React from "react";
import Footer from "./footer";
import ControlledCarousel from "./header";
import Body from "./body";
import SecondBody from "./secondBody";
const LandingPage =()=>{
    return(
        <>
        <ControlledCarousel/>
        <br/>
        <Body/>
        <br/>
        <SecondBody/>
        <Footer/>
        
        </>
    )
}
export default LandingPage