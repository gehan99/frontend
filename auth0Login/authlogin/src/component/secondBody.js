import React from "react";

const SecondBody = () => {
    return (
        <>
            <img src="https://www.stirworld.com/images/article_gallery/the-new-alibaba-shanghai-campus-by-skidmore-owings-merrill-in-shanghai-china-alibaba-shanghai-campus-skidmore-owings-merrill-stirworld-220806035837.jpg" className="img-fluid" alt="Responsive image" />
            <br />
            <div className="container">
                <br/>
                <p>ALIBABA GROUP’S MISSION IS TO MAKE IT EASY TO DO BUSINESS ANYWHERE.
                    We enable businesses to transform the way they market, sell and operate and improve their efficiencies. We provide the technology infrastructure and marketing reach to help merchants, brands, retailers and other businesses to leverage the power of new technology to engage with their users and customers and operate in a more efficient way. We also empower enterprises with our leading cloud infrastructure and services and enhanced work collaboration capabilities to facilitate 
                    their digital transformation and to support the growth of their businesses.</p>
            </div>

        </>
    )
}
export default SecondBody