import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
// import reportWebVitals from './reportWebVitals';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
// import { AuthProvider } from './context/auth'======


// import { NavBarProvider } from './context/navBarcontext'

import { Auth0Provider } from '@auth0/auth0-react'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import allReducer from './redux/reducer';

const mystore = createStore(allReducer)

const domain = process.env.REACT_APP_AUTH0_DOMAIN;
const clientId = process.env.REACT_APP_AUTH0_CLIENT_ID;
// console.log("auth provider --------------------------------->", <AuthProvider />)
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    {/* <NavBarProvider> */}
    {/* <AuthProvider> */}
    <Auth0Provider
      domain={domain}
      clientId={clientId}
      redirectUri={window.location.origin}
      audience="this is unique identifier"
      scope="openid profile email"
    >
      <Provider store={mystore}>
        <App />
      </Provider>
    </Auth0Provider>
    {/* </AuthProvider> */}
    {/* </NavBarProvider> */}
  </React.StrictMode>

  // <Auth0Provider
  //   domain={domain}
  //   clientId={clientId}
  //   redirectUri={window.location.origin}
  //   audience="this is unique identifier"
  //   scope="openid profile email"
  // >
  //   <App />
  // </Auth0Provider>

);
// reportWebVitals();
