import React, { useState, useEffect } from "react";
import { Navigate, Outlet } from "react-router-dom";
import apiservice from "./component/services/apiservices";


const useAuth = () => {
    const [auth,setAuth] = useState(null);
    useEffect(() => {
        const checkToken = async () => {
            const res = await apiservice.getDataAdmin()
            console.log("apiservice adminauth",res)
            setAuth(res)
        }
        checkToken();
    },[]);
    return  auth;
};


const UserAdminprivateroute =  () => {
    const useAuthData =  useAuth();
    if (useAuthData === null)
    return null;
    console.log("useAuth----->",useAuthData)
    return useAuthData ? <Outlet /> : <Navigate to="/LogingErrorMessage" />;

};


export default UserAdminprivateroute;