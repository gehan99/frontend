import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form"
import axios from "axios";
import { useAuth0 } from '@auth0/auth0-react'
import MyDataSet from "../api/api";

const AdminLandingPage = () => {
    const { getAccessTokenSilently,user } = useAuth0()
    const [UpdateUserDetails, setUpdateUserDetails] = useState(false)
    const [admindata, setAdminData] = useState('')
    const [allproduct, setAllProduct] = useState([])
    const [allUsers,setAllUsers] = useState(0)
    const [allOrders,setAllOrders] = useState(0)
    // const [onlineUsers,setOnlineUsers] = useState(0)
    const [tokenData,setToken]= useState('')
    const a = 0;
    useEffect(() => {
        // const a = 0
        accessTokenData()
        const adminId = localStorage.getItem("uid")
        axios.get(`http://localhost:3001/user/${adminId}`,{
            headers: {
                authorization: `Bearer ${tokenData}`,
            }
        }).then((res) => {
            console.log("admin -->", res.data.data)
            setAdminData(res.data.data)
        }).catch((error) => {
            console.log(error)
        })

        axios.get('http://localhost:3001/product',{
            headers: {
                authorization: `Bearer ${tokenData}`,
            }
        }).then((res) => {
            console.log("admin all productsssss-->", res.data.data)
            setAllProduct(res.data)
        }).catch((error) => {
            console.log(error)
        })

       
        const GetAllUsers = async ()=>{
         const res = await  MyDataSet.getUserDetails(tokenData)
            
                console.log("all users res ",typeof(res))
                    console.log("admin all users-->", res.length)
                    setAllUsers(res.length)
                    for (let index = 0; index < res.length; index++) {
                        console.log("for loop users===>",res[index].fname)
                        if (res[index].loginStatus==="active") {
                             a = 1
                            console.log("if condition ===>--------------000000000000",a)
                        }
                    }
                
          
        }

        GetAllUsers()

        // axios.get('http://localhost:3001/user',{
        //     headers: {
        //         authorization: `Bearer ${tokenData}`,
        //     }
        // }).then((res) => {
        //     console.log("admin all users-->", res.data.length)
        //     setAllUsers(res.data.length)
        //     for (let index = 0; index < res.data.length; index++) {
        //         console.log("for loop users===>",res.data[index].fname)
        //         if (res.data[index].loginStatus==="active") {
        //              a = 1
        //             console.log("if condition ===>--------------000000000000",a)
        //         }
                
        //     }
        // }).catch((error) => {
        //     console.log(error)
        // })





        axios.get('http://localhost:3001/order',{
            headers: {
                authorization: `Bearer ${tokenData}`,
            }
        }).then((res) => {
            console.log("admin all users-->", res.data.length)
            setAllOrders(res.data.length)
        }).catch((error) => {
            console.log(error)
        })
    },[])


    const accessTokenData = async()=>{
        const token = await getAccessTokenSilently()
        setToken(token)
    }
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm();

    const onSubmit = async (data) => {
        const uid = localStorage.getItem("uid")
        console.log("data uid==>", uid)
        try {
            const responce = await axios.put(`http://localhost:3001/user/${uid}`, data,{
                headers: {
                    authorization: `Bearer ${tokenData}`,
                }
            }
            );
            console.log("responce", responce.data.data)
            setUpdateUserDetails(false)
        } catch (error) {
            console.log(error)
        }
    }


    return (
        <>
            <br />
            <div className="container-fluid">
                <div className="row">
                    {UpdateUserDetails == false ? <> <div className="col" >
                        <div className="shadow p-3 mb-5 bg-white rounded card" style={{ width: '18rem', height: '35rem' }}>
                            <div className="card-body">
                                <h5 className="card-title">Admin</h5>
                                <div className="row">
                                    <div className="col-5">Name :</div>
                                    <div className="col">{admindata.fname}</div>
                                </div>
                                <div className="row">
                                    <div className="col-5">Email :</div>
                                    <div className="col">{admindata.email}</div>
                                </div>
                                <div className="row">
                                    <div className="col-5">Mobile :</div>
                                    <div className="col">{admindata.mobile}</div>
                                </div>
                                <div className="row">
                                    <div className="col-5">C_ID :</div>
                                    <div className="col">CD2022{admindata.id}</div>
                                </div>
                                <br />
                                <button className="btn btn-primary" onClick={() => setUpdateUserDetails(true)} >updateUser</button>
                                <br /><br />
                                <h5 className="card-title">ItemList</h5>
                                <div className="row">
                                    <div className="col-5">Moters</div>
                                    <div className="col">1000</div>
                                </div>
                                <div className="row">
                                    <div className="col-5">Electronics</div>
                                    <div className="col">1000</div>
                                </div>
                                <div className="row">
                                    <div className="col-5">Art</div>
                                    <div className="col">1000</div>
                                </div>
                                <div className="row">
                                    <div className="col-5">Home</div>
                                    <div className="col">1000</div>
                                </div>
                                <div className="row">
                                    <div className="col-5">Business</div>
                                    <div className="col">1000</div>
                                </div>
                                <div className="row">
                                    <div className="col-5">Sport</div>
                                    <div className="col">1000</div>
                                </div>
                                <div className="row">
                                    <div className="col-5">Phone</div>
                                    <div className="col">1000</div>
                                </div>
                                <div className="row">
                                    <div className="col-5">Laptops</div>
                                    <div className="col">1000</div>
                                </div>
                                <div className="row">
                                    <div className="col-5">Foods</div>
                                    <div className="col">1000</div>
                                </div>
                            </div>
                        </div>
                    </div></> : <> <div className="col" >
                        <div className="shadow p-3 mb-5 bg-white rounded card" style={{ width: '18rem', height: '32rem' }}>
                            <div className="card-body">
                                <h5 className="card-title">Admin</h5>
                                <div className="card-login">
                                    <div className="card" style={{ width: '30rem', }}>
                                        <div className="card-body">
                                            <h5 className="card-title">Add User Details</h5>
                                            <br />
                                            <form onSubmit={handleSubmit(onSubmit)}>
                                                <div className="mb-3">
                                                    <label htmlFor="exampleInputEmail1" className="form-label">
                                                        user Name
                                                    </label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        {...register("fname", {
                                                            required: "Email is Required", pattern: {
                                                            }
                                                        })}
                                                    />
                                                </div>
                                                <div className="mb-3">
                                                    <label htmlFor="exampleInputEmail1" className="form-label">
                                                        mobile number
                                                    </label>
                                                    <input
                                                        type="number"
                                                        className="form-control"
                                                        {...register("mobile", {
                                                            required: "Email is Required", pattern: {

                                                            }
                                                        })}
                                                    />
                                                </div>

                                                <div className="mb-3">
                                                    <label htmlFor="exampleInputPassword1" className="form-label">
                                                        Street
                                                    </label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        {...register("street", {

                                                        })}
                                                    />
                                                </div>

                                                <button type="submit" className="btn btn-primary">
                                                    Add
                                                </button>
                                            </form>
                                            <div style={{ paddingTop: "1%" }}>
                                                <button className="btn btn-primary" onClick={() => setUpdateUserDetails(false)}>
                                                    Back
                                                </button></div>

                                        </div>
                                    </div>
                                    <br /><br />
                                </div>
                            </div>
                        </div>
                    </div></>}
                    <div className="col" >
                        <div className="row">
                            <div className="shadow p-3 mb-5 bg-white rounded card" style={{ width: '23rem', height: '35rem' }}>
                                <div className="card-body">
                                    <h6 className="card-subtitle mb-2 text-muted">Products quntity</h6>
                                    <table class="table table-borderless">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">ProductName</th>
                                                <th scope="col">Quntity</th>
                                                <th scope="col">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {allproduct.map((td, i) => (
                                                <tr key={td.id}>
                                                    <td>{i + 1}</td>
                                                    <td>{td.pname}</td>
                                                    <td>{td.quntity}</td>
                                                    <td>
                                                        {td.quntity < 100 ? <><span class=" badge bg-danger">empty</span></> :
                                                            <><span class="badge bg-success">available</span></>}</td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col">
                        <div className="row">
                            <div className="shadow p-3 mb-5 bg-white rounded card" style={{ width: '23rem', height: '35rem' }}>
                                <div className="card-body">
                                    <h6 className="card-subtitle mb-2 text-muted">Summery</h6>
                                    {/* <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> */}
                                    <div className="row">
                                    <div className="col-6">Number of Users</div>
                                    <div className="col-6">{allUsers}</div>
                                </div>
                                <div className="row">
                                    <div className="col-6">Online users</div>
                                    <div className="col-6">1</div>
                                </div>
                                <div className="row">
                                    <div className="col-6">Offline users</div>
                                    <div className="col-6">7</div>
                                </div>
                                <div className="row">
                                    <div className="col-6">Available Orders</div>
                                    <div className="col-6">3</div>
                                </div>
                                <div className="row">
                                    <div className="col-6">All orders</div>
                                    <div className="col-6">{allOrders}</div>
                                </div>
                                <br/><br/>
                                <div style={{padding:"2%"}}>
                                  <img className="card-img-top" src="https://i.pcmag.com/imagery/reviews/02lLbDwVdtIQN2uDFnHeN41-11.fit_lim.size_1050x591.v1569480019.jpg" alt="Card image cap" />
                                </div>   
                                {/* gehan@gmail.com                */}
                                {/* It0773270956@#1 */}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </>
    )
}

export default AdminLandingPage