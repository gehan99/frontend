import React, { useEffect, useState } from "react";
import MyDataSet from "../api/api";
import Axios from "axios";
import jwt_decode from "jwt-decode";
import { useAuth0 } from '@auth0/auth0-react'
import LogingErrorMessage from "./loginErrormessage";

const MyProfile = () => {
    const { getAccessTokenSilently,user } = useAuth0()
   
    console.log("---->", MyDataSet.user + `${16}`)
    const [userData, setUserData] = useState('')
    const [tokenData,setToken]= useState('')
    
    useEffect(() => {
        // const decodedUid = jwt_decode(localStorage.getItem("token")).id;
        userDetails();
       
    })

const userDetails = async()=>{
    const token = await getAccessTokenSilently()
    setToken(token)
    const decodedUid = localStorage.getItem("uid");

    Axios.get(`http://localhost:3001/user/${decodedUid}`,
        {
            headers: {
                // Authorization:localStorage.getItem('token')
                authorization: `Bearer ${token}`,
                // userId:user
            }
    }
    ).
        then(res => {
            console.log(res.data.data);
            setUserData(res.data.data)
        }).catch(err => {
            console.log(err)
        })
}

    return (
        <>
    {tokenData==0 ?<><LogingErrorMessage/>
        </>:<><section className="vh-100" style={{ backgroundColor: '#f4f5f7' }}>
                <div className="container py-5 h-100">
                    <div className="row d-flex justify-content-center align-items-center h-100">
                        <div className="col col-lg-6 mb-4 mb-lg-0">
                            <div className="card mb-3" style={{ borderRadius: '.5rem' }}>
                                <div className="row g-0">
                                    <div className="col-md-4 gradient-custom text-center text-white" style={{ borderTopLeftRadius: '.5rem', borderBottomLeftRadius: '.5rem' }}>
                                        <img src="https://media.istockphoto.com/id/1130884625/vector/user-member-vector-icon-for-ui-user-interface-or-profile-face-avatar-app-in-circle-design.jpg?s=612x612&w=0&k=20&c=1ky-gNHiS2iyLsUPQkxAtPBWH1BZt0PKBB1WBtxQJRE=" alt="Avatar" className="img-fluid my-5" style={{ width: '80px' }} />
                                        <h5>Marie Horwitz</h5>
                                        <p>Web Designer</p>
                                        <i className="far fa-edit mb-5" />
                                    </div>
                                    <div className="col-md-8">
                                        <div className="card-body p-4">
                                            <h6>Information</h6>
                                            <hr className="mt-0 mb-4" />
                                            <div className="row pt-1">
                                                <div className="col-6 mb-3">
                                                    <h6>First Name</h6>
                                                    <p className="text-muted">{userData.fname}</p>
                                                </div>
                                                <div className="col-6 mb-3">
                                                    <h6>Last Name</h6>
                                                    <p className="text-muted">{userData.lname
                                                    }</p>
                                                </div>
                                            </div>
                                            <div className="row pt-1">
                                                <div className="col-6 mb-3">
                                                    <h6>Email</h6>
                                                    <p className="text-muted">{userData.email}</p>
                                                </div>
                                                
                                            </div>
                                            <div className="row pt-1">
                                                
                                                <div className="col-6 mb-3">
                                                    <h6>Phone</h6>
                                                    <p className="text-muted">{userData.mobile
                                                    }</p>
                                                </div>
                                            </div>
                                            <div className="row pt-1">
                                                <div className="col-6 mb-3">
                                                    <h6>Address</h6>
                                                    <p className="text-muted">{userData.city},{userData.state},{userData.street}</p>
                                                </div>
                                            </div>
                                            <div className="row pt-1">
                                                <div className="col-6 mb-3">
                                                    <h6>Zipcode</h6>
                                                    <p className="text-muted">{userData.zipcode}</p>
                                                </div>
                                            </div>
                                            {/* <div className="d-flex justify-content-start">
                                                <a href="#!"><i className="fab fa-facebook-f fa-lg me-3" /></a>
                                                <a href="#!"><i className="fab fa-twitter fa-lg me-3" /></a>
                                                <a href="#!"><i className="fab fa-instagram fa-lg" /></a>
                                            </div> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section></>}
            
        </>
    )
}

export default MyProfile