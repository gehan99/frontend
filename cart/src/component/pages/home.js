import React, { useEffect, useState } from "react";
import ItemCard from './homeComponents/itemCard'
import Axios from 'axios';
import PostService from "../../context/product-service"
import FooterCompnent from "../footer/footer";
import { useAuth0 } from '@auth0/auth0-react'

// import { Row } from "react-bootstrap";
// import axiosUrl from '../api/api'

const Home = () => {
    const { getAccessTokenSilently,user } = useAuth0()
    const token =  getAccessTokenSilently()
    console.log("token home page product",token)
   
    const [posts, setPosts] = useState([]);
    const [publicPost, setPublicPost] = useState([]);

    useEffect(() => {
        PostService.getAllpublicPost().then(
            (response) => {
                setPublicPost(response.data)
            },
            (error) => {
                console.log(error)
            }
        );
        getAllProducts()
        // 
        
        // Axios.get('http://localhost:3001/product', {
        //     // headers: {
        //     //     // Authorization:localStorage.getItem('token')
        //     //     authorization: `Bearer ${token}`
        //     // }
        // }).
        //     then(res => {
        //         console.log("all product",res);
        //         setPosts(res.data)
        //     }).catch(err => {
        //         console.log(err)
        //     });


        PostService.getAllpublicPost().then(
            (response) => {
                setPublicPost(response.data);
            },
            (err) => {
                console.log(err)
            }
        )
    },)


    const getAllProducts = async ()=>{
        const token = await getAccessTokenSilently()
        console.log("token home page product",token)
        Axios.get('http://localhost:3001/product', {
            headers: {
                // Authorization:localStorage.getItem('token')
                authorization: `Bearer ${token}`,
                userId:user
            }
        }).
            then(res => {
                console.log("all product",res);
                setPosts(res.data)
            }).catch(err => {
                console.log(err)
            });
    }

    return (
        <>
            <div style={{ padding: "2%" }}></div>
            <section className="py-2 container">
                <div className="row" >
                    {
                        posts.map(post => <div className="col" key={post.id}><ItemCard
                            id={post.id}
                            image={post.productimageUrl}
                            pname={post.pname}
                            pnumber={post.pnumber}
                            description={post.description}
                            price={post.price}
                            warranty={post.warranty}
                            title={post.title}
                            subtitle={post.subtitle}
                            quntity={post.quntity}
                        /></div>)
                    }
                </div>
            </section>
            <FooterCompnent />
        </>
    );
}
export default Home;