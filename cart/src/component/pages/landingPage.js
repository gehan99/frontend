import React,{useEffect, useState} from 'react';
import FooterCompnent from '../footer/footer';
import BannerComponent from '../nav/banner';
import HolidayGifts from '../nav/holidayGifts';
import Trendingproducts from '../nav/trending';
import Advertisement from '../nav/addverticement';
import axios from 'axios';
import { useAuth0 } from '@auth0/auth0-react'
axios.defaults.withCredentials = true;

const LandingPage = () => {
    const{loginWithPopup,loginWithRedirect,logout,user,isAuthenticated,getAccessTokenSilently}= useAuth0()
    // const url ="http://localhost:3001/user"
   const[state,setState]=useState('')
    useEffect(()=>{
        try {
            tokenVal()
        } catch (error) {
            console.log(error.message)
        }
       
    })


   const tokenVal = async()=>{
    const token = await getAccessTokenSilently()
    console.log("get token --->",token)
    axios.get('http://localhost:3001/product', {
        headers: {
            // Authorization:localStorage.getItem('token')
            authorization: `Bearer ${token}`
        }
    }).
        then(res => {
            console.log("all product",res.data);
            // setPosts(res.data)
        }).catch(error => {
            console.log(error)
        });
    setState(token)
   }
    return (
        <>
            <div >
                {/* <div>
                    {state}
                </div> */}
                <div>
                    <BannerComponent />
                </div>
                <div>
                    <HolidayGifts />
                </div>
                <div>
                    <Trendingproducts />
                </div>
                <div>
                    <Advertisement />
                </div>
                <div>
                    <FooterCompnent />
                </div>
            </div>
        </>
    )

}
export default LandingPage