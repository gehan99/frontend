import React, { useEffect, useState } from "react";
import Axios from "axios";
import { useAuth0 } from '@auth0/auth0-react'

const AddProducts = () => {
    const url = "http://localhost:3001/product"
    const archiveDataUrl ="http://localhost:3001/archiveProduct"
    const { getAccessTokenSilently,user } = useAuth0()
    const [viewProduct, setViewProduct] = useState(false)
    const [viewAllProducts, SetviewAllProducts] = useState(true)
    const [archiveData, setarchiveData] = useState(true)
    const [getAllProducts, setAllProducts] = useState([]);
    const [getAllAchiveProducts, setAllAchiveProducts] = useState([]);
    const [AddOneProduct, setAddProduct] = useState('')
    const [tokenData,setToken]= useState('')

    useEffect(() => {
        // getAllProducts
        accessTokenData()
        Axios.get(url,{
            headers: {
                authorization: `Bearer ${tokenData}`,
            }
        }).then((res) => {
            console.log("all products:", res.data)
            setAllProducts(res.data)
        }).catch((err) => {
            console.log(err)
        })

        Axios.get(archiveDataUrl,{
            headers: {
                authorization: `Bearer ${tokenData}`,
            }
        }).then((res)=>{
            setAllAchiveProducts(res.data)
        }).catch((err)=>{
            console.log(err)
        })

    })

    const accessTokenData = async()=>{
        const token = await getAccessTokenSilently()
        setToken(token)
    }

    const AddProduct = () => {
        const AddUrl = "http://localhost:3001/product"
        Axios.post(AddUrl,{
            headers: {
                authorization: `Bearer ${tokenData}`,
            }
        }).then((res) => {
            console.log("AddUrl products:", res)
            setAddProduct(res.data)
            // setAllProducts(res.data)
        }).catch((err) => {
            console.log(err)
        })
    }


    
    const DeleteProducts = (id) => {
        console.log("clicked delete button", id)
        const deleteUrl = `http://localhost:3001/product/${id}`
        Axios.delete(deleteUrl, {
            "hideItem": true
        },{
            headers: {
                authorization: `Bearer ${tokenData}`,
            }
        }).then((res) => {
            console.log("Delete products:", res)
            // setAllProducts(res.data)
        }).catch((err) => {
            console.log(err)
        })
    }
    const HideProducts = (id) => {
        console.log("clicked delete button", id)
        const deleteUrl = `http://localhost:3001/productData/${id}`
        Axios.put(deleteUrl, {
            "hideItem": true
        },{
            headers: {
                authorization: `Bearer ${tokenData}`,
            }
        }).then((res) => {
            console.log("Delete products:", res)
            // setAllProducts(res.data)
        }).catch((err) => {
            console.log(err)
        })
    }

    const ShowHideData = (id)=>{
        console.log("clicked show hide button", id)
        const deleteUrl = `http://localhost:3001/productData/${id}`
        Axios.put(deleteUrl, {
            "hideItem": false
        },{
            headers: {
                authorization: `Bearer ${tokenData}`,
            }
        }).then((res) => {
            console.log("show products:", res)
            // setAllProducts(res.data)
        }).catch((err) => {
            console.log(err)
        })
    }
    return (
        <>
            {viewAllProducts == true ?
                <>
                    <div className="container"
                        style={{ paddingBottom: "2%", paddingTop: "2%" }}>
                        <button type="submit" className="btn btn-primary"
                            onClick={() => { SetviewAllProducts(false) }}>View all products</button></div>
                </> :
                <>
                    <div className="container"
                        style={{ paddingBottom: "2%", paddingTop: "2%" }}>
                        <button type="submit" className="btn btn-primary"
                            onClick={() => { SetviewAllProducts(true) }}>Add Product</button></div>
                </>}

            {viewAllProducts == true ?
                <>
                    <div className="container">
                        {viewProduct == false &&
                            <>
                                <div className="card">
                                    <div className="card-body">
                                        <form>
                                            <div className="form-group">
                                                <input type="file" className="form-control-file" id="exampleFormControlFile1" />
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="exampleInputEmail1">Product Id</label>
                                                <input type="" className="form-control" id="Product" aria-describedby="emailHelp" />
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="exampleInputPassword1">ProductName</label>
                                                <input type="text" className="form-control" id="ProductName" />
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="exampleInputPassword1">Description</label>
                                                <input type="text" className="form-control" id="Description" />
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="exampleInputPassword1">Title</label>
                                                <input type="text" className="form-control" id="Title" />
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="exampleInputPassword1">SubTitle</label>
                                                <input type="text" className="form-control" id="SubTitle" />
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="exampleInputPassword1">Quntity</label>
                                                <input type="text" className="form-control" id="Quntity" />
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="exampleInputPassword1">Price</label>
                                                <input type="text" className="form-control" id="Price" />
                                            </div>
                                            <div className="form-check">
                                            </div>
                                            <div className="row">
                                                <div className="col-1"><button type="submit" className="btn btn-primary">Submit</button></div>
                                                <div className="col-1"> <button type="submit" className="btn btn-primary">New</button></div>
                                                <div className="col-1"> <button className="btn btn-primary" onClick={() => setViewProduct(true)}>View</button></div>
                                            </div>
                                        </form>
                                    </div>
                                </div></>}

                        {/* view product */}
                        {viewProduct == true && 
                        <>
                            <div className="text-center" style={{ paddingLeft: "35%" }}>
                            <div className="card shadow p-3 mb-5 bg-white rounded" style={{ width: '18rem' }}>
                                <img className="card-img-top" src="https://celltronics.lk/wp-content/uploads/2021/09/Apple-iPhone-13-Pro-Max-graphite.jpg" alt="Card image cap" />
                                <div className="card-body">
                                    <h5 className="card-title">Card title</h5>
                                    <p className="card-text"></p>
                                </div>
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item">Cras justo odio</li>
                                    <li className="list-group-item">Dapibus ac facilisis in</li>
                                    <li className="list-group-item">Vestibulum at eros</li>
                                </ul>
                                <div className="card-body">
                                    <a href="#" className="card-link">Card link</a>
                                    <a href="#" className="card-link">Another link</a>
                                </div>
                                <div>
                                    <button type="submit" className="btn btn-primary" onClick={() => setViewProduct(false)}>New</button>
                                </div>
                                <br />
                                <div>
                                </div>
                                <br />
                            </div>
                        </div>
                        </>}
                    </div>
                </> :
                <>
                    <div className="container">
                        <div className="card">
                            {archiveData == true ? <><div className="col-1" style={{ padding: "2%" }} onClick={() => setarchiveData(false)}> <button type="submit" className="btn btn-primary">Archive</button></div>
                                {/* get all product details */}
                                <div className="card-body">
                                    <div className="card shadow p-3 mb-5 bg-white rounded">
                                        <div className="card-header">
                                            <h4>All Products</h4>
                                        </div>
                                        <div className="card-body">
                                            <table className="table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Title</th>
                                                        <th scope="col">SubTitle</th>
                                                        <th scope="col">Image</th>
                                                        <th scope="col">ProductName</th>
                                                        <th scope="col">Description</th>
                                                        <th scope="col">Price</th>
                                                        <th scope="col">Quntity</th>
                                                        <th scope="col">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {getAllProducts.map((tb, i) => (
                                                        <tr key={tb.id}>
                                                            <td>{i + 1}</td>
                                                            <td>{tb.title}</td>
                                                            <td>{tb.subtitle}</td>
                                                            <td> <img className="card-img-top" style={{ height: "auto" }} src={tb.productimageUrl} alt="Card image cap" /></td>
                                                            <td>{tb.pname}</td>
                                                            <td>{tb.description}</td>
                                                            <td>{tb.price}.00</td>
                                                            <td>{tb.quntity}</td>
                                                            <td>
                                                                <div className="row" style={{ padding: "2%" }}> <div className="col-3"><button type="button" class="btn btn-success">Quntity</button></div></div>
                                                                <div className="row" style={{ padding: "2%" }}><div className="col-3"><button type="button" class="btn btn-primary">Price</button></div></div>
                                                                <div className="row" style={{ padding: "2%" }}><div className="col-3"><button type="button" class="btn btn-warning" onClick={() => HideProducts(tb.id)}>Hide</button></div></div>
                                                                <div className="row" style={{ padding: "2%" }}><div className="col-3"><button type="button" class="btn btn-danger" onClick={() => DeleteProducts(tb.id)}>Delete</button></div></div>
                                                            </td>
                                                        </tr>
                                                    ))}
                                                </tbody>
                                            </table>
                                            <div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* end */}
                            </> :
                                <>
                                {/* achive product data table */}
                                    <div className="col-3" style={{ padding: "1%" }}> <button type="submit" className="btn btn-primary" onClick={() => setarchiveData(true)}>All Product</button></div>
                                    <div className="card-body">
                                        <div className="card shadow p-3 mb-5 bg-white rounded">
                                            <div className="card-header">
                                                <h4>All Achive Products</h4>
                                            </div>
                                            <div className="card-body">
                                                <table className="table">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">#</th>
                                                            <th scope="col">Title</th>
                                                            <th scope="col">SubTitle</th>
                                                            <th scope="col">Image</th>
                                                            <th scope="col">ProductName</th>
                                                            <th scope="col">Description</th>
                                                            <th scope="col">Price</th>
                                                            <th scope="col">Quntity</th>
                                                            <th scope="col">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {getAllAchiveProducts.map((tb, i) => (
                                                            <tr key={tb.id}>
                                                                <td>{i + 1}</td>
                                                                <td>{tb.title}</td>
                                                                <td>{tb.subtitle}</td>
                                                                <td> <img className="card-img-top" style={{ height: "auto" }} src={tb.productimageUrl} alt="Card image cap" /></td>
                                                                <td>{tb.pname}</td>
                                                                <td>{tb.description}</td>
                                                                <td>{tb.price}.00</td>
                                                                <td>{tb.quntity}</td>
                                                                <td>
                                                                    <div className="row" style={{ padding: "2%" }}> <div className="col-3"><button type="button" class="btn btn-success">Quntity</button></div></div>
                                                                    <div className="row" style={{ padding: "2%" }}><div className="col-3"><button type="button" class="btn btn-primary">Price</button></div></div>
                                                                    <div className="row" style={{ padding: "2%" }}><div className="col-3"><button type="button" class="btn btn-primary" onClick={() => ShowHideData(tb.id)}>Add</button></div></div>
                                                                </td>
                                                            </tr>
                                                        ))}
                                                    </tbody>
                                                </table>
                                                <div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {/* end */}
                                </>}
                        </div>
                    </div>
                </>}






        </>
    )
}

export default AddProducts