import React from "react";
import {Link} from 'react-router-dom'
import { IoIosArrowBack } from "react-icons/io";
// import { useAuth0 } from '@auth0/auth0-react'


const ViewItemHeader =()=>{
  // const { user} = useAuth0()
 
    return(
        <>
        <nav className="navbar navbar-expand-lg navbar-light bg-light ">
        <div className="container-fluid">
          <Link className="navbar-brand" to="/home"><IoIosArrowBack size='2rem' color="blue"/>Back</Link>
          
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav ">
            </ul>
          </div>
          <div className="collapse navbar-collapse nav justify-content-end" id="navbarNavDropdown">
            <ul className="navbar-nav">
              <li className="nav-item">
                <Link className="nav-link active" aria-current="page" ></Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" ></Link>
              </li>
              
            </ul>
          </div>
        </div>
      </nav>
        </>
    )
}

export default ViewItemHeader;