import React, { useState, useEffect } from "react";
import HeaderNavBar from './secondNav';
import { useSearchParams, useNavigate, createSearchParams } from 'react-router-dom';
import Axios from 'axios';
import jwt_decode from "jwt-decode";
import { useAuth0 } from '@auth0/auth0-react'
import LogingErrorMessage from "../loginErrormessage";
import { async } from "@firebase/util";
import MyDataSet from "../../api/api";
// import { useAuth0 } from '@auth0/auth0-react'
// import ProductServices from '../../../context/product-service';
// import authService from "../../../context/auth-services";

const OneItem = () => {
    console.log("-------------->local uid",MyDataSet.LocalDataUid.uid)
    const { getAccessTokenSilently, user } = useAuth0()
    const navigate = useNavigate();
    const url = "http://localhost:3001/order"
    const orderurl = "http://localhost:3001/orderItem"
    const [number, setNumber] = useState(1)

    const [userId, setUserId] = useState(" ")
    const [orderid, setOrderId] = useState("")
    const [cartId, setCartId] = useState("")
    const [posts, setPosts] = useState('');
    const [tokenData, setToken] = useState('')


    const [searchparams] = useSearchParams();

    const priductId = parseInt(searchparams.get("id"))

    console.log("33333-productid-->", priductId)


    const [privateD, setPrivateD] = useState([])


    const increse = () => {
        const val = number + 1
        setNumber(val)
    }
    const decrese = () => {
        if (number > 1) {
            const val = number - 1
            setNumber(val)
        }

    }




    


    const openOrderTable = async (id) => {
        console.log("click -->----user id------>openOrderTable ", userId)
        await Axios.post(url, {
            userId: userId,
            apprual: false,
            address: "matara-colombo-hakmana",
            mobile: 4567891230,
            TotalPrice: 4560123,
            status: "not_paid"
        }, {headers: {authorization: `Bearer ${tokenData}`,}
        }

        ).then(res => {
            console.log("original--->", res.data.data.id)
            console.log("iiii", res.data)
            if (res.data.data.id) {
                setOrderId(res.data.data.id)
                console.log("if part", res.data.data.id)
            } else {
                setOrderId(res.data.data)
                console.log("else part", res.data.data)
            }
            console.log("useridusestate ", userId)
            
        })
        
        console.log ("order item api -----")
        await Axios.post(orderurl, {
            pId: priductId,
            orderId: orderid,
            quntity: number,
            reqWarranty: "1 month-1",
            warrantyStatus: "yes",
            availability: "no",
        }).then(res => {
            console.log("eeeeeeeeee", res.data)
        })

        
        navigate({
            pathname: "/cart",
            search: createSearchParams({
                id: posts.id,
                OrderId: orderid,
                uid: userId
            }).toString()
        })
    }
    
    useEffect(() => {
        GetOneProduct();
        AddData()
        getAccessToken()
    },[])


    const AddData = async () => {
        // const decoded = await jwt_decode(localStorage.getItem("token")).id;
        const decoded =  localStorage.getItem("uid");
        setUserId(decoded)
    }


    const GetOneProduct = async () => {
        await Axios.get(`http://localhost:3001/product/${priductId}`,).
            then(res => {
                const posts = res.data.data
                setPosts(posts)
                console.log(posts);
            }).catch(err => {
                console.log(err)
            })
    }

    const getAccessToken = async () => {
        const token = await getAccessTokenSilently()
        setToken(token)
    }


















    return (
        <>
            <HeaderNavBar />
            {tokenData == 0 ? <><LogingErrorMessage /></> : <>
                <div className="container" style={{ paddingTop: '20px' }}>
                    <div className="row">
                        <div className="col-5">
                            <div className="card shadow p-3 mb-5 bg-white rounded" style={{ paddingBottom: '10px', height: '104%' }}>
                                <div className="card-body">
                                    <img src={posts.productimageUrl} className="img-fluid" alt="..." />
                                </div>
                            </div>
                        </div>

                        <div className="col">
                            <div className="card shadow p-3 mb-5 bg-white rounded" style={{ paddingBottom: '1px', height: '35%' }}>
                                <div className="card-body">
                                    <div className="row">
                                        <h3>{posts.pname}</h3>
                                    </div>
                                    <div className="row">
                                        <h5>{posts.description}</h5>
                                    </div>
                                </div>
                            </div>
                            <div className="card shadow p-3 mb-5 bg-white rounded " style={{ height: '58%' }}>
                                <div className="card-body">
                                    <div className="continer">
                                        <div className="row">
                                            <div className="col-2">Price </div>
                                            <div className="col-8"> Rs. {posts.price}  </div>
                                        </div>

                                        <div className="row" style={{ paddingTop: '10px' }}>
                                            <div className="col-2"></div>
                                            <div className="col-8">
                                                <div className="row">
                                                    <div className="col-2"><button type="button" className="btn btn-primary" onClick={decrese}>-</button></div>
                                                    <div className="col-2">
                                                        <div className="row"><p className="text-center">{number}</p></div></div>
                                                    <div className="col-2"><button type="button" className="btn btn-primary" onClick={increse}>+</button> </div>
                                                    <div className="col-6">
                                                        <div className="container"><button type="button" className="btn btn-success" style={{ paddingLeft: 11 }} onClick={openOrderTable}> Add to cart</button> </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div></>}
        </>
    );
}
export default OneItem;