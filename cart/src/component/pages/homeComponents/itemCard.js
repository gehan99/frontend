import React from "react";
import "./item.css";
import {useNavigate ,createSearchParams} from 'react-router-dom'

const Itemcard = (props) => {
   

    const navigate = useNavigate();
    const openProfile=(id)=>{
        navigate({
            pathname:"/item",
             search :createSearchParams({
            id:props.id
    }).toString()
    })
    }
    

    return (
        <>
            <div className="col-11 col-md-6 col-lg-3 mx-0 mb-3 ">
                <div className="">
                    <div className="card shadow p-3 mb-5 bg-white rounded" style={{ width: '18rem' }}>
                        <img src={props.image} className="card-img-top" alt="..." />
                        <div className="card-body">
                            <h5 className="card-title">{props.pname}</h5>
                            <p className="card-text">{props.description}</p>
                            <p className="card-text">Rs.{props.price}.00</p>
                            <p className="card-text">{props.quntity}</p>
                            <button type="button" className="btn btn-primary" onClick={
                                // () => setState(true)
                                openProfile
                            }>view</button>

                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Itemcard;
