import React from "react";
import HeaderNavBar from '../homeComponents/secondNav'

const ViewOrders= () => {
    return (
        <>
        <HeaderNavBar/>
            <div className="container-fluid" style={{ padding: '10px' }}>
                <div className="card shadow p-3 mb-5 bg-white rounded">
                    <div className="card-header">
                    <h3>ORDERS HISTORY</h3>
                    </div>
                    <div className="card-body">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Product</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Quntity</th>
                                    <th scope="col">Warranty</th>
                                    <th scope="col">States</th>
                                    <th scope="col">Price</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><span class="badge bg-primary">pending</span><span class="badge bg-secondary">on-hold</span></td>
                                    <td></td>
                                    <td><button type="button" class="btn btn-danger" href="/view_orders">Delete</button></td>
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><span class="badge bg-danger">reject</span>
                                        <span class="badge bg-warning text-dark">inprogress</span></td>
                                    <td></td>
                                    <td><button type="button" class="btn btn-danger" href="/view_orders">Delete</button></td>
                                </tr>

                            </tbody>
                        </table>
                        <div>
                        </div>
                    </div>
                </div>

            </div>
        </>
    );
}

export default ViewOrders;