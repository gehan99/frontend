import React,{useState} from 'react'
import {Navigate} from 'react-router-dom'


const OrderRegister = () => {
    const [stste,setState]=useState(false)
    if(stste){
        return<Navigate to="/order"/>
    }
    return (
        <>
            <div className='container' >
                <div style={{ paddingTop: "3%" }}></div>
                <div className="card shadow p-3 mb-5 bg-white rounded" >
                    <div className='card-header'><h4>CREATE ORDER</h4></div>
                    <div className="card-body">
                        <div className='row'>
                            <div className='col'>
                                <div className="card shadow p-3 mb-5 bg-white rounded">
                                    <div className="card-body">
                                        <form>
                                            <div className="mb-3">
                                                <label htmlFor="exampleInputEmail1" className="form-label">Name</label>
                                                <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                                            </div>
                                            <div className="mb-3">
                                                <label htmlFor="exampleInputPassword1" className="form-label">Email</label>
                                                <input type="email" className="form-control" id="exampleInputPassword1" />
                                            </div>
                                            <div className="mb-3">
                                                <label htmlFor="exampleInputEmail1" className="form-label">Address</label>
                                                <input type="text " className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />

                                            </div>
                                            <div className="mb-3">
                                                <label htmlFor="exampleInputEmail1" className="form-label">Mobile</label>
                                                <input type="number" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                                            </div>
                                            <div className='row'>
                                            <div className='col-3'><button type="submit" className="btn btn-primary">Submit</button></div>
                                            <div className='col-3'><button type="button" className="btn btn-primary">cancel</button></div>
                                        </div>
                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div className='col'>
                                <div className="card shadow p-3 mb-5 bg-white rounded">
                                    <div className="card-body">
                                        <div>
                                            <div className="text-center">
                                                <img src="https://uxwing.com/wp-content/themes/uxwing/download/e-commerce-currency-shopping/order-placed-purchased-icon.png" className="rounded" alt="..." />
                                            </div>
                                        </div>
                                        <br/><br/>
                                        <div className='row'>
                                            {/* <div className='col-3'><button type="button" className="btn btn-primary">Primary</button></div> */}
                                            <div className='col-3'><button type="button" className="btn btn-primary" onClick={()=>setState(true)}>Naxt</button></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )


}

export default OrderRegister;
