import React,{useEffect, useState} from "react";
import HeaderNavBar from '../homeComponents/secondNav';
import {useSearchParams} from 'react-router-dom';
import Axios from "axios";
import FooterCompnent from "../../footer/footer";


const OrderTable= () => {
    // const url = "http://localhost:3001/orderItem";
    const [tabledata,setTableData]= useState([])
   
    useEffect(()=>{
        const decodedUid = localStorage.getItem("uid");
        Axios.get(`http://localhost:3001/ordersingalusers/${decodedUid}`).then(res => {
                 console.log("---------33333333377777--->",res.data);
                setTableData(res.data)
            }).catch(err => {
                console.log(err)
            },[])
        // CreateOrder();

    })
    const [searchparams] = useSearchParams();
    console.log(searchparams.get("id"))
    const priductId = searchparams.get("id")
    console.log("order table ->",priductId)

    // const [data, setData] = useState({
    //     user: "",
    //     apprual: "",
    //     address: "",
    //     mobile: "",
    //     TotalPrice: "",
    //     status: "",
    // })
    
    // const url="http://localhost:3001/product";

   

    // const CreateOrder=()=>{
    //     console.log("iiiiiiiiiiiiii")
    //     Axios.post("http://localhost:3001/order",{
    //         user: 13,
    //         apprual: "no",
    //         address: "matara",
    //         mobile: 4444444,
    //         TotalPrice: 10000,
    //         status: "no",
    //     }).then(res => {
    //         console.log(res.data.succes)
    //     })
    // }

    

    
    return (
        <>
        <HeaderNavBar/>
            <div className="container-fluid" style={{ padding: '10px' }}>
                <div className="card shadow p-3 mb-5 bg-white rounded">
                    <div className="card-header">
                        <h3>ORDERS</h3>
                    </div>
                    <div className="card-body">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Order</th>
                                    {/* <th scope="col">Vender</th> */}
                                    {/* <th scope="col">Delivery Date</th> */}
                                    <th scope="col">Status</th>
                                    <th scope="col"></th>
                                    <th scope="col">Total</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                {tabledata.map((tb,i)=>(
                                    <tr key={tb.id}>

                                        <td>{i+1}</td>
                                        <td>{tb.createdAt}</td>
                                        <td>OR{tb.id}</td>
                                        <td>{tb.apprual== 1?<><span class="badge bg-success">Approved</span></>:<><span class="badge bg-primary">pending</span></>}</td>
                                        <td>{tb.apprual}</td>
                                        {/* <td><span class="badge bg-primary">Not showing</span></td> */}
                                       
                                        <td>Rs {tb.TotalPrice}.00</td>
                                        <td>
                                            {tb.apprual== 0 ?<> <button type="button" class="btn btn-success">view</button></>:<><button type="button" class="btn btn-primary">pay</button></>}
                                              <button type="button" class="btn btn-danger">Delete</button>
                                              
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                        <div>
                        </div>
                    </div>
                </div>

            </div>
            <FooterCompnent/>
        </>
    );
}

export default OrderTable;