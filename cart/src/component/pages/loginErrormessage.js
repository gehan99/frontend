import React, { useEffect } from "react";
import { useAuth0 } from '@auth0/auth0-react'
const LogingErrorMessage = () => {
    const {  loginWithRedirect,user} = useAuth0()
    useEffect(() => {
        console.log("user Details=====>", user)
        const userToken = localStorage.getItem("token");
        console.log(userToken)
    })
    return (
        <>
            <div className="card-login">
                <div className="card" style={{ width: '30rem', padding: "2%" }}>
                    <div className="card-body">
                        {/* <h5 className="card-title">LOGIN</h5> */}
                        <br />
                        <img className="card-img-top" src="https://media.istockphoto.com/id/1281150061/vector/register-account-submit-access-login-password-username-internet-online-website-concept.jpg?s=612x612&w=0&k=20&c=9HWSuA9IaU4o-CK6fALBS5eaO1ubnsM08EOYwgbwGBo=" alt="Card image cap" />
                        <button className="btn btn-primary text-center" onClick={loginWithRedirect}>
                            Login
                        </button>
                    </div>
                </div>
                <br /><br />
            </div>
        </>
    )
}

export default LogingErrorMessage