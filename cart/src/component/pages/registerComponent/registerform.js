import React from "react";
import './register.css'
// import { Card, Form, Button, Row, Col } from 'react-bootstrap';
import Axios from 'axios';
import { useForm } from "react-hook-form"
import { useNavigate } from 'react-router-dom'

const RegisterForm = () => {

    //---------------------------------------
    const navigate = useNavigate();
    // const initialValues = { fname: "", lname: "", email: "", address: "", mobile: "", password: "",confirmpassword:"" }
    const url = "http://localhost:3001/user"
    // const [formErrors, setformErrors] = useState({})
    // const [isSubmit, setIsSubmit] = useState(false)
    // const [data, setData] = useState(initialValues)

    // function handle(e) {
    //     const newdata = { ...data }
    //     newdata[e.target.id] = e.target.value
    //     setData(newdata)
    //     console.log(newdata)
    // }

    // const navigate = useNavigate();
    // function handlesubmit(e) {
    //     e.preventDefault();
    //     setformErrors(validate(data));
    //     setIsSubmit(true)
    //     Axios.post(url, {
    //         fname: data.fname,
    //         lname: data.lname,
    //         email: data.email,
    //         address: data.address,
    //         mobile: data.mobile,
    //         password: data.password,
    //     }).then(res => {
    //         console.log(res.data.succes)
    //         if (res.data.succes == true) {
    //             navigate("/login")
    //         }
    //     })
    // }

    // useEffect(()=>{
    //     console.log("user effect run")
    //     console.log(formErrors);
    //     if(Object.keys(formErrors).length ===0 && isSubmit){
    //         console.log(setData)
    //     }
    // })
    // const clearData=()=>{

    // }
    //for validation
    // const validate = (values) => {
    //     const errors ={}
    //     const regex=/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
    //     if(!values.fname){
    //         errors.fname="Fname is required."
    //     }
    //     if(!values.lname){
    //         errors.lname="Lname is required."
    //     }
    //     if(!values.email){
    //         errors.email="Email is required."
    //     }
    //     if(!values.address){
    //         errors.address="Address is required."
    //     }
    //     if(!values.mobile){
    //         errors.mobile="Mobile number is required."
    //     }
    //     if(!values.password){
    //         errors.password="Password is required."
    //     }
    //     if(!values.confirmpassword){
    //         console.log("password==========>",values.password)
    //       if(values.password=== values.clearData){
    //         errors.password="not true"
    //         }
    //         errors.password="Password is required."
    //     }

    //     return errors;
    // }

    //---------------------------------------

    // const [formValue,setFormValue]=useState(initialValues)

    const {
        register,
        handleSubmit,
        watch,
        formState: { errors },
    } = useForm();
    
    // console.log("data--->+", register.fname)
    const onSubmit = (data) => {
        console.log("final-->", data)
        Axios.post(url, data
        //     {
        //     fname: data.fname,
        //     lname: data.lname,
        //     email: data.email,
        //     address: data.address,
        //     mobile: data.mobile,
        //     password: data.password,
        // }
        
        ).then(res => {
            console.log(res.data.succes)
            if (res.data.succes === true) {
                navigate("/login")
            }
        })
    }

    const password = watch("password")
    // const errorData = errors;

    // console.log("err--->", errors.fname)
    return (
        <>
            <div className="reg">
                <div className="card shadow-sm p-3 mb-5 bg-body rounded" style={{ width: '30rem', padding: "2%", backgroundColor: "dark" }}>
                    <div className="card-body">
                        <h5 className="card-title">SIGN UP</h5>
                        <br />
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className="mb-3 row">
                                <div className="col">
                                    <label htmlFor="exampleInputEmail1" className="form-label">
                                        First Name
                                    </label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        aria-describedby="emailHelp"
                                        {...register("fname", {
                                            required: "First name is required.", pattern: {
                                                value: /^[0-9A-Za-z\s\-]+$/,
                                                message: "invalid name."
                                            }
                                        })}
                                    />
                                    {errors.fname && (<small className="text-danger">{errors.fname.message}</small>)}
                                </div>
                                <div className="col">
                                    <label htmlFor="exampleInputPassword1" className="form-label">
                                        Last Name
                                    </label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        {...register("lname", {
                                            required: "Last Name is required.", pattern: {
                                                value: /^[0-9A-Za-z\s\-]+$/,
                                                message: "Invalid name."
                                            }
                                        })}
                                    />
                                    {errors.lname && (<small className="text-danger">Last name is reqired</small>)}
                                </div>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="exampleInputPassword1" className="form-label">
                                    Email
                                </label>
                                <input
                                    type="email"
                                    className="form-control"
                                    {...register("email", {
                                        required: "Email is required.", pattern: {
                                            value: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                            message: "Invalid email."
                                        }
                                    })}
                                />
                                {errors.email && <small className="text-danger">{errors.email.message}</small>}
                            </div>
                            {/* -- */}

                            <div className="mb-3 row">
                                <div className="col">
                                    <label htmlFor="exampleInputEmail1" className="form-label">
                                        Street
                                    </label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        aria-describedby="emailHelp"
                                        {...register("street", {
                                            required: "street name is required.", pattern: {
                                                // value: /[A-Z][a-z]*[^A-Za-z0-9]+/g,
                                                value: /^[A-Z][a-z]*(?: [A-Z][a-z]*)*$/,
                                                message: "Invalid street name.First letter must be a capital letter"
                                            }
                                        })}
                                    />
                                    {errors.street && (<small className="text-danger">{errors.street.message}</small>)}
                                </div>
                                <div className="col">
                                    <label htmlFor="exampleInputPassword1" className="form-label">
                                        City
                                    </label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        {...register("city", {
                                            required: "City Name is required.", pattern: {
                                                value: /^[A-Z][a-z]*(?: [A-Z][a-z]*)*$/,
                                                message: "Invalid city name. First letter must be a capital letter "
                                            }
                                        })}
                                    />
                                    {errors.city && (<small className="text-danger">{errors.city.message}</small>)}
                                </div>
                            </div>
                            <div className="mb-3 row">
                                <div className="col">
                                    <label htmlFor="exampleInputEmail1" className="form-label">
                                        State
                                    </label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        aria-describedby="emailHelp"
                                        {...register("state", {
                                            required: "State name is required.", pattern: {
                                                // value: /^[a-zA-Z]+$/g,
                                                value:/^[A-Z][a-z]*(?: [A-Z][a-z]*)*$/,
                                                message: "Invalid name.First letter must be a capital letter"
                                            }
                                        })}
                                    />
                                    {errors.state && (<small className="text-danger">{errors.state.message}</small>)}
                                </div>
                                <div className="col">
                                    <label htmlFor="exampleInputPassword1" className="form-label">
                                        ZIP code
                                    </label>
                                    <input
                                        type="number"
                                        className="form-control"
                                        {...register("zipcode", {
                                            required: "Zipcode is required.", pattern: {
                                                value: /^[0-9A-Za-z\s\-]+$/,
                                                message: "Invalid zipcode."
                                            }
                                        })}
                                    />
                                    {errors.zipcode && (<small className="text-danger">{errors.zipcode.message}</small>)}
                                </div>
                            </div>
                                    

                            {/* <div className="mb-3">
                                <label htmlFor="exampleInputPassword1" className="form-label">
                                    Address
                                </label>
                                <input
                                    type="text"
                                    className="form-control"
                                    {...register("address", { required: "Address is required." })}
                                />
                                {errors.address && (<small className="text-danger">{errors.address.message}</small>)}
                            </div> */}
                            <div className="mb-3">
                                <label htmlFor="exampleInputPassword1" className="form-label">
                                    Mobile
                                </label>
                                <input
                                    type="number"
                                    className="form-control"
                                    {...register("mobile", {
                                        required: "Mobile number is required.", pattern: {
                                            value: /^(?:7|0|(?:\+94))[0-9]{9,10}/,
                                            message: "Invalid mobile number."
                                        },
            
                                    })}
                                />
                                {errors.mobile && (<small className="text-danger">{errors.mobile.message}</small>)}
                            </div>
                            <div className="mb-3">
                                <label htmlFor="exampleInputPassword1" className="form-label">
                                    Password
                                </label>
                                <input
                                    type="password"
                                    className="form-control"
                                    {...register("password", {
                                        required: "Password is required", pattern: {
                                            value: /^\S*(?=\S{6,})(?=\S*\d)(?=\S*[A-Z])(?=\S*[a-z])(?=\S*[!@#$%^&*? ])\S*$/,
                                            message: "admin1@regexpattern.com"
                                        },
                                        minLength: {
                                            value: 8,
                                            message: "Password length is lower than the supported password length."
                                        }, maxLength: {
                                            value: 25,
                                            message: "Password length is greater than the supported password length."
                                        }
                                    })}
                                />
                                {errors.password && <small className="text-danger">{errors.password.message}</small>}
                            </div>
                            <div className="mb-3">
                                <label htmlFor="exampleInputPassword1" className="form-label">
                                    Confirm Password
                                </label>
                                <input
                                    type="password"
                                    className="form-control"
                                    {...register("confirmpassword", {
                                        required: "Confirmpassword is required",
                                        validate: (value) =>
                                            value === password || "The password do not match"
                                    })}
                                />
                                {errors.confirmpassword && <small className="text-danger">{errors.confirmpassword.message}</small>}
                            </div>
                            <button type="submit" className="btn btn-primary">
                                Submit
                            </button>
                        </form>

                    </div>
                </div>
                {/* <Card bg="dark" style={{ width: '30rem', padding: "2%", backgroundColor: "darkcyan" }} >
                    <Card.Title className="card-Register-header"><h3>SIGN UP</h3></Card.Title>
                    <Card.Body>
                        <Form onSubmit={(e) => submit(e)}>
                            <Row>
                                <Col>
                                    <Form.Group className="mb-3" controlId="fname">
                                        <Form.Label className="card-Register-header">First Name</Form.Label>
                                        <Form.Control placeholder="First name" onChange={(e) => handle(e)} type="text" value={data.fname} />
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Form.Group className="mb-3" controlId="lname" >
                                        <Form.Label className="card-Register-header">Last Name</Form.Label>
                                        <Form.Control placeholder="Last name" onChange={(e) => handle(e)} type="text" value={data.lname} />
                                    </Form.Group>
                                </Col>
                            </Row>
                            <Form.Group className="mb-3" controlId="email"  >
                                <Form.Label className="card-Register-header">Email Address</Form.Label>
                                <Form.Control type="email" placeholder="Enter email" onChange={(e) => handle(e)} value={data.email} required/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="address"  >
                                <Form.Label className="card-Register-header">Address</Form.Label>
                                <Form.Control type="text" placeholder="Enter Address" onChange={(e) => handle(e)} value={data.address} required/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="mobile" >
                                <Form.Label className="card-Register-header">Mobile Number</Form.Label>
                                <Form.Control type="number" placeholder="Enter Mobile Number" onChange={(e) => handle(e)} value={data.number} required/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="password">
                                <Form.Label className="card-Register-header">Password</Form.Label>
                                <Form.Control type="password" placeholder="Password" onChange={(e) => handle(e)} value={data.password} required/>
                            </Form.Group>
                            <Button variant="primary" type="submit"> Submit</Button>
                        </Form>
                    </Card.Body>
                </Card> */}
            </div>
        </>
    );
}

export default RegisterForm;