
import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import Axios from 'axios';
import { useAuth0 } from '@auth0/auth0-react'

const Admin_user_table = () => {
    const url = "http://localhost:3001/user"
    const { getAccessTokenSilently,user } = useAuth0()
    const [show, setShow] = useState(false);
    const [selects,setSeleects]=useState('')
    const [selectuid,setUid]=useState('')
    const [tokenData,setToken]= useState('')

    const handleClose = () =>{
        console.log("show--->",selects,selectuid)
        Axios.put( `http://localhost:3001/user/${selectuid}`,{
            userType:selects
        },{
            headers: {
                authorization: `Bearer ${tokenData}`,
            }
        })
        setShow(false);
    } 
    const handleShow = (a) =>{
        setUid(a)
        setShow(true);
    } 
    const [userDetails, setUserDetails] = useState([])

    useEffect(() => {
        accessTokenData()
        Axios.get(url,{
            headers: {
                authorization: `Bearer ${tokenData}`,
            }
        }).then((res) => {
            console.log(res.data)
            setUserDetails(res.data)
        }).catch((err) => {
            console.log("err", err)
        })
    },[])

    const accessTokenData = async()=>{
        const token = await getAccessTokenSilently()
        setToken(token)
    }
    const deleteUsers = (id)=>{
        const userUrl= `http://localhost:3001/user/${id}`
        Axios.delete(userUrl,{
            headers: {
                authorization: `Bearer ${tokenData}`,
            }
        }).then((res) => {
            console.log("delete",res.data) 
        }).catch((err) => {
            console.log("err", err)
        })
    }
    return (
        <><div className="container-fluid">
            <div className="card">
                <div className="card-body">
                    <table className="table caption-top">
                        <caption>List of users {selects}</caption>
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col"></th>
                                <th scope="col">UserName</th>
                                <th scope="col">Email</th>
                                <th scope="col">UserType</th>
                                {/* <th scope="col">U</th> */}
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {userDetails.map((td, j) => (
                                <tr style={{fontFamily:"sans-serif"}}>
                                    <th scope="row">{j+1}</th>
                                    <td>{td.loginStatus == "online"? <span class="badge bg-success">Online</span>:<span class="badge bg-danger">Offline</span>}</td>
                                    <td>{td.fname} {td.lname}</td>
                                    <td>{td.email}</td>
                                    <td>{td.userType === 1?"User":"admin"}</td>
                                    <td>
                                        <div >
                                            <div className="row">
                                                <div className="col-3">
                                                    <Button variant="primary" onClick={()=>handleShow(td.id)}>
                                                        View
                                                    </Button>
                                                </div>
                                                <div className="col-3">
                                                <Button variant="danger"onClick={()=>deleteUsers(td.id)} >
                                                        Delete
                                                    </Button>
                                                    {/* <button type="button" class="btn btn-danger" onClick={()=>deleteUsers(td.id)}>Delete</button> */}
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Change user type</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Select aria-label="Default select example" value={selects} onChange={(e)=>setSeleects( e.target.value)}>
                        <option>select user type</option>
                        <option value="1">user</option>
                        <option value="2">admin</option>
                    </Form.Select>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={()=>handleClose(selects)}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={handleClose}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>


        </>
    )
}

export default Admin_user_table