import React from "react";
import NavigationBar from '../nav/navBar';
import { Route, BrowserRouter, Routes } from 'react-router-dom'
import Home from './home';
import Login from "./login";
import Register from "./register";
import Cart from "./cart";
import Item from "./homeComponents/oneitem";
import Order from "./orders";
import ViewOrders from "./orderComponent/viewOrders";
import OrderRegister from "./orderComponent/orderform";
import LandingPage from './landingPage';
import ErrerPage from "./err";
import Admindata from "./admin"
import AdminOrders from "./admin.orders";
import AdminProducts from "./admin.product";
import AdminUsers from "./admin.user";
import MyProfile from "./profile";
import LogingErrorMessage from "./loginErrormessage";
// import RequireAuth from "./requireAuth";
// import {PrivetRoute} from "./userprivateroute"
// import {Authprovider} from "../../context/auth";
//----->
import Userprivateroute from "../../userprivateroute";
import UserAdminprivateroute from "../../userAdminPrivateRoute";

// import { useAuth0 } from '@auth0/auth0-react'


const User = () => {
    // const {NavigationBar}= AlldataSets()
    // const { isAuthenticated,loginWithRedirect} = useAuth0()

    // const ROLES = {
    //     'user':1,
    //     'admin':2
    // }
    return (
        <>
            <BrowserRouter>
                <NavigationBar />
                <Routes>
                    {/* public routes */}
                    <Route path="/" element={<LandingPage />}></Route>
                    <Route path="/login" element={<LogingErrorMessage />}></Route>
                    <Route path="/register" element={<Register />}></Route>

                    <Route element={<UserAdminprivateroute />}>
                        <Route path="/admin" element={<Admindata />}></Route>
                        <Route path="/admin-orders" element={<AdminOrders />}></Route>
                        <Route path="/admin-product" element={<AdminProducts />}></Route>
                        <Route path="/admin-users" element={<AdminUsers />}></Route>
                        <Route path="/*" element={<LogingErrorMessage />}></Route>
                    </Route>

                    <Route path="/*" element={<ErrerPage />}></Route>

                    {/* private route */}
                    {/* <Route element={<RequireAuth allowedRoles={[1]}/>} > */}
                    
                    <Route element={<Userprivateroute />} >
                        <Route path="/home" element={<Home />} />
                        <Route path="/item" element={<Item />} />
                        <Route path="/cart" element={<Cart />} />
                        <Route path="/myProfile" element={<MyProfile />} />
                        MyProfile
                        <Route path="/orderRegister" element={<OrderRegister />} />
                        <Route path="/vieworders" element={<ViewOrders />} />
                        <Route path="/order" element={<Order />} />
                        <Route path="/*" element={<LogingErrorMessage />}></Route>
                    </Route>
                </Routes>
            </BrowserRouter>
        </>
    );
}

export default User