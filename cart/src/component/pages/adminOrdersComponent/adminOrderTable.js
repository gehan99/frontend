import React, { useState,useEffect } from "react";
import HeaderNavBar from "../homeComponents/secondNav";
import FooterCompnent from "../../footer/footer";
import Axios from "axios";
import { useAuth0 } from '@auth0/auth0-react'


const AdminOrderTable = ()=>{
    const { getAccessTokenSilently,user } = useAuth0()
    const[tabledata,setTableData]=useState([])
    const[newOrders,setNewOrders]=useState(false)
    const [tokenData,setToken]= useState('')

    useEffect(()=>{
        accessTokenData()
        Axios.get('http://localhost:3001/order',{
            headers: {
                authorization: `Bearer ${tokenData}`,
            }
        }).then(res => {
                 console.log("233-----all orders--4------->",res.data);
                setTableData(res.data)
               
            }).catch(err => {
                console.log(err)
            })

            Axios.get('http://localhost:3001/ordernew',{
                headers: {
                    authorization: `Bearer ${tokenData}`,
                }
            }).then(res => {
                 console.log("-----all orders34----43--->",res.data);
                //  setNewOrders(res.data)
            }).catch(err => {
                console.log(err)
            })
        // CreateOrder();
    })


    const accessTokenData = async()=>{
        const token = await getAccessTokenSilently()
        setToken(token)
    }



    const Approved = (id,apprualStates)=>{
        setNewOrders(!apprualStates)
       const  data = {
            apprual:newOrders
        }
        Axios.put(`http://localhost:3001/order/${id}`,data,{
            headers: {
                authorization: `Bearer ${tokenData}`,
            }
        }).then(res => {
       }).catch(err => {
           console.log(err)
       })

    }
    return(
        <>
       <HeaderNavBar/>
            <div className="container-fluid" style={{ padding: '10px' }}>
                <div className="card shadow p-3 mb-5 bg-white rounded">
                    <div className="card-header">
                        <h3>Admin Order Confirmation <div className="col-3"></div></h3>
                    </div>
                    <div className="card-body">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Order</th>
                                    <th scope="col"></th>
                                    <th scope="col">states</th>
                                    <th scope="col">user states</th>
                                    <th scope="col">Amount</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                {tabledata.map((tb,i)=>(
                                    <tr key={tb.id}>
                                        <td>{i+1}</td>
                                        <td>{tb.createdAt}</td>
                                        <td>OR{tb.id}</td>
                                        <td>{tb.apprual}</td>
                                        <td>{tb.apprual==1?<><span class="badge bg-success">Approved</span></>:<><span class="badge bg-warning">Not approved</span></>}</td>
                                        <td>{tb.status==="pay"?<span class="badge bg-primary">pending</span>:<span class="badge bg-secondary">close</span>}</td>
                                        <td> <p style={{color:"black",fontWeight:"bold"}}>Rs {tb.TotalPrice}.00</p></td>
                                        <td>
                                        <div className="row">
                                            <div className="col">
                                            {tb.apprual==0?
                                            <> 
                                            <div className="col-4"><button type="button" class="btn btn-primary" onClick={()=>Approved(tb.id,tb.apprual)}>
                                                Approved {tb.apprual}</button>
                                            </div>
                                            </>:
                                            <> 
                                            <div className="col-3"><button type="button" class="btn btn-warning" onClick={()=>Approved(tb.id,tb.apprual)}>Not Approved {tb.apprual}</button>
                                            </div>
                                            </> }
                                            </div>
                                            <div className="col-3" > <button type="button" className="btn btn-success">view</button></div>
                                           
                                            <div className="col-3" style={{paddingTop:"%"}}><button type="button" class="btn btn-danger">Reject</button></div>
                                        </div>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                        <div>
                        </div>
                    </div>
                </div>
            </div>



           
            <FooterCompnent/>
        </>
    )
}
export default AdminOrderTable