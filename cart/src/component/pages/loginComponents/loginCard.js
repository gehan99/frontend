import React, { useContext, useState } from "react";
import './login.css'
import axios from 'axios';
import { useForm } from "react-hook-form"
import { useNavigate, useLocation } from 'react-router-dom'
import { createUserWithEmailAndPassword } from 'firebase/auth'
import { auth } from "../../../firebase-config"
import { async } from "@firebase/util";
// import jwt_decode from "jwt-decode";
axios.defaults.withCredentials = true;

const LoginCard = () => {

    const navigate = useNavigate();
    const location = useLocation();

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    const RegisterUsers = async () => {
        try {
            const registerUsers = await createUserWithEmailAndPassword(auth, email, password)
            console.log("firebase res", registerUsers)
        } catch (error) {
            alert(error)
        }

    }

    const from = location.state?.from?.pathname || "/";
    const urldata = 'http://localhost:3001/login'

    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm();

    const onSubmit = async (data) => {
        console.log("data email==>", data.email)
        console.log("data password==>", data.password)
        try {
            const responce = await axios.post(urldata, data,
            );
            console.log("responce", responce.data.data)
            localStorage.setItem("token", responce.data.data)
            navigate(from, { replace: true })
            // https://www.youtube.com/watch?v=oUZjO00NkhY
            // https://www.youtube.com/watch?v=oUZjO00NkhY
        } catch (error) {
            console.log(error)
        }
    }







    return (
        <>
            <div className="card-login">
                <div className="card" style={{ width: '30rem', padding: "2%" }}>
                    <div className="card-body">
                        <h5 className="card-title">LOGIN</h5>
                        <br />
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className="mb-3">
                                <label htmlFor="exampleInputEmail1" className="form-label">
                                    Email address
                                </label>
                                <input
                                    type="email"
                                    className="form-control"
                                    {...register("email", {
                                        required: "Email is Required", pattern: {
                                            value: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                            message: "invalid email"
                                        }
                                    })}
                                />
                                {errors.email && <span className="text-danger">{errors.email.message}</span>}

                            </div>
                            <div className="mb-3">
                                <label htmlFor="exampleInputPassword1" className="form-label">
                                    Password
                                </label>
                                <input
                                    type="password"
                                    className="form-control"
                                    {...register("password", {
                                        // required: "password is Required", pattern: {
                                        //     value: /^\S*(?=\S{6,})(?=\S*\d)(?=\S*[A-Z])(?=\S*[a-z])(?=\S*[!@#$%^&*? ])\S*$/,
                                        //     message: "admin1@regexpattern.COM"
                                        // },
                                        // minLength: {
                                        //     value: 8,
                                        //     message: "Password length is lower than the supported password length"
                                        // }, maxLength: {
                                        //     value: 25,
                                        //     message: "Password length is greater than the supported password length"
                                        // }
                                    })}
                                />
                                {errors.password && <span className="text-danger">{errors.password.message}</span>}
                            </div>
                            {/* <p style={{ textAlign: "right" }}>Forgot password?</p> */}

                            <button type="submit" className="btn btn-primary">
                                Login
                            </button>
                            <p style={{ textAlign: "center" }}>Create new <a style={{ textAlign: "center" }} href="/register">Account.</a></p>
                        </form>
                    </div>
                </div>
            </div>




            <div className="card-login">
                <div className="card" style={{ width: '30rem', padding: "2%" }}>
                    <div className="card-body">
                        <h5 className="card-title">LOGIN</h5>
                        <br />

                        {/* <div className="mb-3">
                            <label htmlFor="exampleInputEmail1" className="form-label">
                                Email address
                            </label>
                            <input
                                type="email"
                                className="form-control"
                                onChange={(event) => {
                                    setEmail(event.target.value)
                                }}

                            />
                            {errors.email && <span className="text-danger">{errors.email.message}</span>}

                        </div> */}
                        {/* <div className="mb-3">
                            <label htmlFor="exampleInputPassword1" className="form-label">
                                Password
                            </label>
                            <input
                                type="password"
                                className="form-control"
                                onChange={(event) => {
                                    setPassword(event.target.value)
                                }}
                            />
                            {errors.password && <span className="text-danger">{errors.password.message}</span>}
                        </div> */}
                        {/* <p style={{ textAlign: "right" }}>Forgot password?</p> */}

                        {/* <button className="btn btn-primary" onClick={RegisterUsers}>
                            Login
                        </button>
                        <p style={{ textAlign: "center" }}>Create new <a style={{ textAlign: "center" }} href="/register">Account.</a></p> */}
                        <button className="btn btn-primary" >
                            Login
                        </button>
                        {/* <p style={{ textAlign: "center" }}>Create new <a style={{ textAlign: "center" }} href="/register">Account.</a></p> */}

                    </div>





                </div>
                <br /><br />
            </div>
        </>
    );
}

export default LoginCard






