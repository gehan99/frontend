import React, { useEffect, useState } from "react";
import HeaderNavBar from '../homeComponents/secondNav';
import { useNavigate, useSearchParams } from 'react-router-dom'
import Axios from "axios";
import jwt_decode from "jwt-decode";
import { useAuth0 } from '@auth0/auth0-react';


const CartTable = () => {
    const { getAccessTokenSilently, user } = useAuth0()
    const [orderlist, setOrderList] = useState([])
    const [total, setTotal] = useState(0)
    const [oIdData, setOIdData] = useState('')
    const [OrderIdData, setOrderIdData] = useState('')
    const [tokenData, setToken] = useState('')
    const [searchparams] = useSearchParams();
    const orderId = parseInt(searchparams.get("OrderId"))
    console.log("order id :", orderId)

    useEffect(() => {
        const decodedUid = localStorage.getItem("uid");
        getAccessToken()
        console.log("second data ", decodedUid)
        Axios.get(`http://localhost:3001/order/${decodedUid}`, {
            headers: {
                authorization: `Bearer ${tokenData}`,
            }
        }).
            then(res => {
                console.log("order details---------->",res.data[0].id)
                Axios.get(`http://localhost:3001/orderItem/${res.data[0].id}`,
                    {
                        headers: {
                            authorization: `Bearer ${tokenData}`,
                        }
                    }).
                    then(res => {
                        console.log("cart product list------------> ", res.data)
                        setOrderList(res.data)
                        setOrderIdData(res.data[0].id)
                        setOIdData(res.data[0].order.id)
                        let a = 0;
                        for (let index = 0; index < res.data.length; index++) {
                            a = a + (res.data[index].quntity * res.data[index].p.price)
                            setTotal(a)
                        }
                    }).catch(err => {
                        console.log(err)
                    });
            }).catch(err => {
                console.log(err)
            });
    },[])

    const getAccessToken = async () => {
        const token = await getAccessTokenSilently()
        setToken(token)
    }

    const navigate = useNavigate();

    const nextPage = async () => {

        console.log("order id-----------> next page==>", oIdData)
        console.log("order id----r-------> next page==>", OrderIdData)

        await Axios.put(`http://localhost:3001/order/${oIdData}`, {
            status: "pay",
            TotalPrice: total
        },
            {
                headers: {
                    authorization: `Bearer ${tokenData}`,
                }
            }).
            then(res => {
                console.log("------------><--------------", res);
                // navigate("/orderRegister")
            }).catch(err => {
                console.log(err)
            })
        navigate("/orderRegister")
    }



    const deteteAddedProducts = (id) => {
        Axios.delete(`http://localhost:3001/orderItem/${id}`, {
            headers: {
                authorization: `Bearer ${tokenData}`,
            }
        }).
            then(res => {
                console.log("after deleting----->",res.data)
                // useEffect()
                // setOrderList(res.data)
            }).catch(err => {
                console.log(err)
            });
    }


    const IncreaseValue = async (id, quntity) => {
        Axios.put(`http://localhost:3001/orderItem/${id}`, {
            quntity: quntity + 1,
        }).
            then(res => {
                console.log("responce updated---> ", res.data)
            }).catch(err => {
                console.log(err)
            });
    }

    const Decreasevalue = (id, quntity) => {
        if (quntity > 1)
            Axios.put(`http://localhost:3001/orderItem/${id}`, {
                quntity: quntity - 1,
            },
                {
                    headers: {
                        authorization: `Bearer ${tokenData}`,
                    }
                }).
                then(res => {
                    console.log("responce updated---> ", res.data)
                }).catch(err => {
                    console.log(err)
                });
    }

    const GoHomepage = ()=>{
        navigate("/home")
    }


    return (
        <>
            <HeaderNavBar />
            <div className="container-fluid" style={{ padding: '10px' }}>
                <div className="card shadow p-3 mb-5 bg-white rounded">
                    <div className="card-header">
                        <h3>CART</h3>
                    </div>
                    <div className="card-body">
                        <div className="table-responsive">
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">img</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Qty</th>
                                        <th scope="col">Price</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {orderlist.map((i, j) => {
                                        return (
                                            <tr>
                                                <th scope="row">{j + 1}</th>
                                                <td><img src={orderlist[j].p.productimageUrl} className="rounded" alt="..." /></td>
                                                <td>{orderlist[j].p.pnumber}</td>
                                                <td>{orderlist[j].p.description}</td>
                                                <td> <div className="row">
                                                    <div className="col"><button type="button" className="btn btn-success" onClick={() => Decreasevalue(orderlist[j].id, orderlist[j].quntity)}>-</button></div>
                                                    <div className="col"><p>{orderlist[j].quntity}</p></div>
                                                    <div className="col"><button type="button" className="btn btn-success" onClick={() => IncreaseValue(orderlist[j].id, orderlist[j].quntity)}>+</button></div></div>
                                                </td>
                                                <td>Rs.{orderlist[j].quntity * orderlist[j].p.price}.00  <p >yyyy</p></td>
                                                <td><button type="button" className="btn btn-danger" onClick={() => deteteAddedProducts(orderlist[j].id)}>Delete</button></td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                        </div>
                        <table className="table table-responsive">
                        </table>
                        {orderlist.length > 0 ?
                            <>
                                <div className="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <h5>Total Price:</h5>
                                    <p>Rs {total}.00</p>
                                </div>
                                <div>
                                    <div className="d-grid gap-2 d-md-flex justify-content-md-end">
                                        <button type="button" className="btn btn-primary">clear</button>
                                        <button type="button" className="btn btn-primary" onClick={nextPage}>Create Order</button>
                                    </div>
                                </div>
                            </> : <>
                                <div>
                                    <div className="d-grid gap-2 d-md-flex justify-content-md-end">
                                        <button type="button" className="btn btn-primary" onClick={GoHomepage}>Add Items</button>
                                    </div>
                                </div></>
                        }



                    </div>
                </div>
            </div>
        </>
    );
}
export default CartTable;