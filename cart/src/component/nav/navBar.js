import React, { useState, useEffect } from "react";
import { FaShoppingCart } from "react-icons/fa";
import { BsHandbag } from "react-icons/bs";
import { Link } from 'react-router-dom';
import apiservices from '../services/apiservices'
import axios from "axios";
import { useAuth0 } from '@auth0/auth0-react'
import {useSelector,useDispatch} from 'react-redux'

const NavigationBar = () => {
  const counter = useSelector(state=>state.userDetails)


  // ------------Auth0-------------------
  const {loginWithRedirect, logout, user, isAuthenticated } = useAuth0()



  // ------------Auth0-------------------
  const [userType, setUserType] = useState(0);
  const [userTypeone, setUserTypeone] = useState(0);
  const [userId, setUserId] = useState('');

  useEffect(() => {
    console.log("user Details=====>",user)
    const userToken = localStorage.getItem("token");
    console.log(userToken)

    if (userToken) {
      if (apiservices.getDataAdmin()) {
        setUserType(1);
      } else {
        setUserType(2);
      }
    }
    getUserdata();
    addUsers();
  },)

  // useEffect(() => {
  //   getUserdata();
  //   addUsers();
  // },)

  async function getUserdata() {
    const getddta = localStorage.getItem("token")
    if (!getddta) {
      console.log("if is 0++++++++",)
    } else {
      console.log("else part=",)
    }
  }


const addUsers = ()=>{
if(isAuthenticated){
  
  const data={
    fname:"user",
    lname:"",
    email:user.email,
    address:"",
    mobile: 56,
    imageUrl:"",
    password:user.sub,
    loginStatus:"online",
    userType:"",
    permition:false
}
  axios.post("http://localhost:3001/user",data).then((res)=>{

  setUserTypeone(res.data.data.userType)
  setUserId(res.data.data.id)
  localStorage.setItem('Name', res.data.data.userType);
  localStorage.setItem('uid', res.data.data.id);

  const uid = localStorage.getItem("uid")
  // localStorage.clear();
  axios.put(`http://localhost:3001/user/${uid}`,{
    loginStatus:"online"
  }).then(()=>{

  }).catch((error)=>{
    console.log("logout ",error)
  })
})


}
}

const UserLogout = async()=>{
  const uid = localStorage.getItem("uid")
  localStorage.clear();
  axios.put(`http://localhost:3001/user/${uid}`,{
    loginStatus:"offline"
  }).then(()=>{

  }).catch((error)=>{
    console.log("logout ",error)
  })
  logout()
}

  return (
    <div>
      <>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <div className="container-fluid">
            <a className="navbar-brand" ><BsHandbag size='2rem' color="blue" />Shopify</a>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon" />
            </button>
            <div className="collapse navbar-collapse" id="navbarNavDropdown">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <Link className="nav-link active" aria-current="page" to='/' >Home</Link>
                </li>
                {/* <p className="nav-link active">counter:{counter}</p> */}
                {userTypeone === 1 && <>
                  <li className="nav-item">
                    <Link className="nav-link active" aria-current="page" to='/myProfile'>MyProfile</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" aria-current="page" to='/home' >Items</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" aria-current="page" to='/order'>My Orders</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" aria-current="page">About</Link>
                  </li>
                </>}

                {userTypeone === 2 && <><li className="nav-item">
                  <Link className="nav-link" aria-current="page" to='/admin'>Main</Link>
                </li>
                  <li className="nav-item">
                    <Link className="nav-link" aria-current="page" to='/admin-product'>Products</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" aria-current="page" to='/admin-orders'>Order List</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" aria-current="page" to='/admin-users'>User List</Link>
                  </li></>}
              </ul>
            </div>
            <div className="collapse navbar-collapse nav justify-content-end" id="navbarNavDropdown">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <Link className="nav-link active" aria-current="page" ></Link>
                </li>

                {isAuthenticated && <><li className="nav-item">
                  <Link className="nav-link">{JSON.stringify(user.given_name)}</Link>
                </li></>}

                {isAuthenticated ? <><li className="nav-item">
                  <Link className="nav-link" onClick={UserLogout}  >logout</Link>
                </li></> : <><li className="nav-item">
                  <Link className="nav-link" onClick={loginWithRedirect}>Login</Link>
                </li></>}
                <li className="nav-item">
                  <Link className="nav-link" to='/cart'><FaShoppingCart size='2rem' color="white" /></Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </>
    </div>
  )
}



export default NavigationBar 