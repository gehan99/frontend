import React from "react";

const BannerComponent = () => {
    return (
        <>
            <div>
                <div style={{height:"80%"}}>
                    <img className="card-img-top" src="https://img.freepik.com/premium-psd/headphone-brand-product-sale-facebook-cover-web-banner-template_161398-292.jpg?w=2000" alt="Card image cap" style={{
                        // position:"fixed",
                        height:400,
                        minWidth: "100%",
                        minHeight:"100%",
                        backgroundSize: "cover",
                        backgroundPosition: "center"
                    }} />
                </div>
            </div>

        </>
    )
}
export default BannerComponent