import React from "react";

const HolidayGifts = ()=>{
    return(
        <>
        
        <div className="container-fluid" style={{padding:"2%"}}> <h4>Score these trending kicks</h4></div>
        <div className="container row text-center" style={{paddingLeft:"10%"}}>
                <div className="col-3"> <img src="https://i.ebayimg.com/images/g/4MEAAOSwwL5i~jO7/s-l640.png" className="img-thumbnail" alt="..." />
                <a>Jordan 1 Retro Low OG UNC Dark Powder Blue</a></div>
                <div className="col-3"> <img src="https://i.ebayimg.com/images/g/IV0AAOSwKx1ipzuc/s-l640.png" className="img-thumbnail" alt="..." />
                <a>Jordan 4 Red Thunder</a></div>
                <div className="col-3"> <img src="https://i.ebayimg.com/images/g/6CYAAOSwCHti~RTB/s-l640.png" className="img-thumbnail" alt="..." />
                <a>Nike Dunk High SB x FroSkate All Love No Hate 2022</a></div>
                <div className="col-3"> <img src="https://i.ebayimg.com/images/g/wNUAAOSw06Bib9Gr/s-l640.png" className="img-thumbnail" alt="..." />
                <a>Nike Dunk Low Retro White Black 2021</a></div>

            </div>
        </>
    )
}

export default HolidayGifts