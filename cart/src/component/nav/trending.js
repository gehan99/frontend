import React from "react";

const Trendingproducts = () => {
    return (
        <>
            <div className="container-fluid"style={{padding:"2%"}}> <h4>New Products. </h4></div>

            <div className="container row text-center" style={{paddingLeft:"10%"}}>
                <div className="col-3"> <img src="https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/FG133?wid=1144&hei=1144&fmt=jpeg&qlt=90&.v=1614983667000" className="img-thumbnail" alt="..." />
                <a>Apple Watch</a></div>
                <div className="col-3"> <img src="https://celltronics.lk/wp-content/uploads/2022/06/02-16-600x600.jpg" className="img-thumbnail" alt="..." />
                <a>Laptops</a></div>
                <div className="col-3"> <img src="https://celltronics.lk/wp-content/uploads/2021/09/Apple-iPhone-13-Pro-Max-graphite.jpg" className="img-thumbnail" alt="..." />
                <a>Mobile</a></div>
                <div className="col-3"> <img src="https://www.dslrpros.com/media/catalog/product/m/2/m200v2_1.jpg?quality=80&bg-color=255,255,255&fit=bounds&height=700&width=700&canvas=700:700" className="img-thumbnail" alt="..." />
                <a>Drone</a></div>

            </div>
        </>
    )
}

export default Trendingproducts