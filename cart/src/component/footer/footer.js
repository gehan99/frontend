import React from "react";

const FooterCompnent = () => {
    return (
        <>
            <footer className="text-center text-lg-start bg-dark text-muted">
                <section className="d-flex justify-content-center justify-content-lg-between p-4 border-bottom">
                    <div className="me-5 d-none d-lg-block">
                        <span>Get connected with us on social networks:</span>
                    </div>
                    <div>
                        <a href className="me-4 link-secondary">
                            <i className="fab fa-facebook-f" />
                        </a>
                        <a href className="me-4 link-secondary">
                            <i className="fab fa-twitter" />
                        </a>
                        <a href className="me-4 link-secondary">
                            <i className="fab fa-google" />
                        </a>
                        <a href className="me-4 link-secondary">
                            <i className="fab fa-instagram" />
                        </a>
                        <a href className="me-4 link-secondary">
                            <i className="fab fa-linkedin" />
                        </a>
                        <a href className="me-4 link-secondary">
                            <i className="fab fa-github" />
                        </a>
                    </div>
                   
                </section>
                {/* Section: Social media */}
                {/* Section: Links  */}
                <section className>
                    <div className="container text-center text-md-start mt-5">
                        {/* Grid row */}
                        <div className="row mt-3">
                            <div className="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                                {/* Links */}
                                <h6 className="text-uppercase fw-bold mb-4">
                                 About
                                </h6>
                                <p>
                                    <a href="#!" className="text-reset">Company info</a>
                                </p>
                                <p>
                                    <a href="#!" className="text-reset">News</a>
                                </p>
                                <p>
                                    <a href="#!" className="text-reset">Investors</a>
                                </p>
                                <p>
                                    <a href="#!" className="text-reset">Careers</a>
                                </p>
                            </div>
                            {/* Grid column */}
                            {/* Grid column */}
                            <div className="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
                                {/* Links */}
                                <h6 className="text-uppercase fw-bold mb-4">
                                  Help & Contact
                                </h6>
                                <p>
                                    <a href="#!" className="text-reset">Seller Center</a>
                                </p>
                                <p>
                                    <a href="#!" className="text-reset">Contact Us</a>
                                </p>
                                <p>
                                    <a href="#!" className="text-reset">Shopify Returns</a>
                                </p>
                                <p>
                                    <a href="#!" className="text-reset">Announcements</a>
                                </p>
                            </div>
                            <div className="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
                                {/* Links */}
                                <h6 className="text-uppercase fw-bold mb-4">
                                    Buy
                                </h6>
                                <p>
                                    <a href="#!" className="text-reset">Registration</a>
                                </p>
                                <p>
                                    <a href="#!" className="text-reset">Bidding & buying help</a>
                                </p>
                                <p>
                                    <a href="#!" className="text-reset">Stores</a>
                                </p>
                                <p>
                                    <a href="#!" className="text-reset">Shopify for Charity</a>
                                </p>
                            </div>
                            {/* Grid column */}
                            {/* Grid column */}
                            <div className="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                                {/* Links */}
                                <h6 className="text-uppercase fw-bold mb-4">Contact</h6>
                                <p><i className="fas fa-home me-3 text-secondary" /> New York, NY 10012, US</p>
                                <p>
                                    <i className="fas fa-envelope me-3 text-secondary" />
                                    info@example.com
                                </p>
                                <p><i className="fas fa-phone me-3 text-secondary" /> + 01 234 567 88</p>
                                <p><i className="fas fa-print me-3 text-secondary" /> + 01 234 567 89</p>
                            </div>
                            {/* Grid column */}
                        </div>
                        {/* Grid row */}
                    </div>
                </section>
                {/* Section: Links  */}
                {/* Copyright */}
                <div className="text-center p-4" style={{ backgroundColor: 'rgba(0, 0, 0, 0.025)' }}>
                    © 2022 Copyright:
                    <a className="text-reset fw-bold" href="https://mdbootstrap.com/">Shopify.com</a>
                </div>
                {/* Copyright */}
            </footer>
        </>
    )
}

export default FooterCompnent