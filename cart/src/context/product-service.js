import axios from "axios"
import authHeader from "./authheader"

const API_URL='/api';

const getAllpublicPost=()=>{
    return axios.get(API_URL+"/public")
}

const getAllprivatcPost=()=>{
    return axios.get(API_URL+"/private",{headers:authHeader()})
}

const postServices={
    getAllpublicPost,
    getAllprivatcPost
};

export default postServices;