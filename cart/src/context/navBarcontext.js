import {createContext, useReducer} from "react";
// const Sample = true
const initialState = {
    Sample:0,
}
const reducer = (state,action)=>{
    return state;
}

export const NavBarContext = createContext();

export const NavBarProvider = ({childrean})=>{
    const value = useReducer(reducer,initialState)
    return <NavBarContext.Provider value={value}>{childrean}</NavBarContext.Provider>
};
