import axios from 'axios'
const API_URL = "http://localhost:3001"

const login = (email,password) => {
    return axios.post(API_URL + "/login", {
        email,
        password
    }).then((responce) => {
        if (responce.data.access_token) {
            localStorage.setItem("user", JSON.stringify(responce.data))
        }

        return responce.data;
    })

}
const logout = () => {
    localStorage.removeItem("user")
}

const getCurrentUser =()=>{
    return JSON.parse(localStorage.getItem("user"))
}

const authService ={
    login,
    logout,
    getCurrentUser
}
export default authService