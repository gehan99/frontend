import React, { useState, useEffect } from "react";
import { Navigate, Outlet } from "react-router-dom";
import apiservice from "./component/services/apiservices";

const useAuth = () => {
    const [auth,setAuth] = useState(null);
    useEffect(() => {
        const checkToken = async () => {
            const res = await apiservice.getData()
            console.log("apiservice auth-->",res)
            setAuth(res)
        }
        checkToken();
    },[]);
    return  auth;
};




const Userprivateroute =  () => {
    const useAuthData =  useAuth();
    if (useAuthData === null)
    return null;
    console.log("useAuth--xd--->",useAuthData)
    return useAuthData ? <Outlet /> : <Navigate to="/login" />;

};


export default Userprivateroute;