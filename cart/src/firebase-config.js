
import { initializeApp } from "firebase/app";
import { getAuth } from 'firebase/auth'
import { getFirestore } from 'firebase/firestore'
// import { getAnalytics } from "firebase/analytics";

const firebaseConfig = {
  apiKey: "AIzaSyBUZr_bJuG5Q7W1-KfCjTrP9Y22ZC1o6us",
  authDomain: "cartweb-7cba6.firebaseapp.com",
  projectId: "cartweb-7cba6",
  storageBucket: "cartweb-7cba6.appspot.com",
  messagingSenderId: "515467784997",
  appId: "1:515467784997:web:6485952e78f44421b3e0cc",
  measurementId: "G-2KCL7TJR1P"
};

// Initialize Firebase

 const app = initializeApp(firebaseConfig)
export const db = getFirestore(app)
export const auth = getAuth(app)

// const analytics = getAnalytics(app);