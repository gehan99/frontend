import isLogged from "./isLoged";
import UserData from "./userData";
import {combineReducers} from 'redux'

const allReducer = combineReducers({
    userDetails:UserData,
    LoginDetails:isLogged
})

export default allReducer